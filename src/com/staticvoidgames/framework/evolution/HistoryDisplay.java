package com.staticvoidgames.framework.evolution;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.staticvoidgames.maze.MazeLevel;

public class HistoryDisplay {

	int length = 500;
	int border = 5;
	int bottom = 100;

	BufferedImage image;

	private JPanel contentPane = new JPanel();
	private JPanel currentRow = new JPanel();

	List<BufferedImage> currentRowImages = new ArrayList<BufferedImage>();

	int populationSize;

	public HistoryDisplay(int populationSize){
		this.populationSize = populationSize;
		
		image = new BufferedImage(populationSize*(length+border*2), 1, BufferedImage.TYPE_INT_RGB);
		
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		currentRow.setLayout(new BoxLayout(currentRow, BoxLayout.X_AXIS));

		contentPane.add(currentRow);

		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					File outputfile = new File("history.png");
					ImageIO.write(image, "png", outputfile);
				}
				catch(Exception ex){
					ex.printStackTrace();
				}
			}

		});


		JFrame frame = new JFrame("History");
		frame.add(contentPane);
		frame.add(saveButton, BorderLayout.SOUTH);
		frame.setSize(500, 500);
		frame.setVisible(true);


	}


	private void addRowToImage(){

		BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight()+currentRowImages.get(0).getHeight(), BufferedImage.TYPE_INT_RGB);
		
		

		Graphics g = newImage.getGraphics();
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, newImage.getWidth(), newImage.getHeight());
		
		
		g.drawImage(image, 0, 0, null);
		
		


		int x = 0;
		int y = image.getHeight();

		for(BufferedImage levelImage : currentRowImages){
			g.drawImage(levelImage, x, y, null);
			x += levelImage.getWidth();
		}
		
		currentRowImages.clear();

		image = newImage;

	}



	public void addLevel(final MazeLevel level, double fitness){
		
		
		
		BufferedImage image = new BufferedImage(length + border*2, length+border*2 + bottom, BufferedImage.TYPE_INT_RGB);
		Graphics imageGraphics = image.getGraphics();
		
		//imageGraphics.setColor(Color.WHITE);
		//imageGraphics.fillRect(0, 0, image.getWidth(), image.getHeight());
		
		//imageGraphics.setColor(Color.BLACK);
		//imageGraphics.fillRect(0, 0, length+border*2, length+border*2);
		
		imageGraphics.setColor(Color.WHITE);
		imageGraphics.fillRect(border, border, length, length);
		imageGraphics.fillRect(border, length+border*2, length, bottom-border);
		
		imageGraphics.translate(border, border);
		
		level.render(imageGraphics, length, length);
		
		imageGraphics.translate(-border, -border);
		
		imageGraphics.setColor(Color.BLACK);
		DecimalFormat df = new DecimalFormat("00.00");
		imageGraphics.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 72));
		imageGraphics.drawString(df.format(fitness/1000), image.getWidth()/2 - border*2 - 50, image.getHeight() - 25);
		
		
		currentRowImages.add(image);

		JPanel panel = new JPanel(){
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				level.render(g, getWidth(), getHeight());
			}
		};

		panel.setSize(100, 100);

		currentRow.add(panel);
		
		if(currentRow.getComponentCount() == populationSize){
			currentRow = new JPanel();
			currentRow.setLayout(new BoxLayout(currentRow, BoxLayout.X_AXIS));
			contentPane.add(currentRow);
			
			addRowToImage();
		}

		contentPane.revalidate();




	}




}
