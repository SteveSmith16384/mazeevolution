package com.staticvoidgames.framework.evolution;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.staticvoidgames.maze.Maze;

/**
 * A framework for performing customizable evolution of parameters meant to supply game content generators.
 */
public class EvolutionFramework {

	/**
	 * A List of parameter arrays that still need to be scored.
	 */
	private List<double[]> unscoredPopulation = new ArrayList<double[]>();

	/**
	 * Map of parameter arrays to their scores.
	 */
	private Map<double[], Double> scoredPopulation = new HashMap<double[], Double>();

	/**
	 * How many individual parameter arrays are in each generation?
	 */
	private int populationSize = 5;

	/**
	 * Whether the best score is the smallest.
	 */
	private boolean minimize = true;
	
	/**
	 * Instance of Random that can be seeded to give predictable results.
	 * Note that the predictability of the framework depends on the predictability
	 * of the user-supplied generator and evaluator.
	 */
	private Random random = new Random();
	
	private int generation = 0;
	
	private Maze maze;

	
	public EvolutionFramework(int parameterLength){
		this(parameterLength, 5);
	}
	
	
	/**
	 * Constructs a new EvolutionFramework with an initially randomized population
	 * of parameter arrays of the specified length.
	 * @param parameterLength
	 */
	public EvolutionFramework(int parameterLength, int populationSize){
		
		this.populationSize = populationSize;
		
		for(int i = 0; i < populationSize; i++){
			
			double[] child = new double[parameterLength];
			
			for(int p = 0; p < parameterLength; p++){
				child[p] = random.nextDouble();
			}
			
			unscoredPopulation.add(child);
			
		}
		
		
	}

	/**
	 * Constructs a new EvolutionFramework using the specified parameter array as the initial parent.
	 * @param defaultParameters
	 */
	public EvolutionFramework(double[] defaultParameters, int populationSize){
		this.populationSize = populationSize;
		
		for(int i = 0; i < populationSize; i++){
			double[] child = copyAndMutate(defaultParameters);
			unscoredPopulation.add(child);
		}
		
		
	}
	
	public void setMaze(Maze maze){
		this.maze = maze;
	}
	
	/**
	 * 
	 * @param p
	 * @param score
	 */
	public void setScore(double[] p, double score){
		//maze.log("Fitness: " + score);
		if (p != null) {
		scoredPopulation.put(p, score);
		}
	}

	public double[] getNext() {
		if(unscoredPopulation.isEmpty()){
			createNextGeneration();
		}
		return unscoredPopulation.remove(0);
	}
	
	
	public double getBestScore(){
		double[] best = getBest();
		return scoredPopulation.get(best);
	}
	
	private void createNextGeneration(){
		generation++;
		
		double[] best = getBest();
		
		DecimalFormat df = new DecimalFormat(".00");
		StringBuilder bestString = new StringBuilder();
		for(double d : best){
			bestString.append(df.format(d));
			bestString.append(", ");
		}
		bestString.setLength(bestString.length()-2);
		
		maze.log("Generation " + generation + " best: [" + bestString + "] (" + scoredPopulation.get(best) + ")");
		maze.log("Mutating best to create new generation.");
		
		//System.out.println();
		//System.out.print("Best: ");
		//for(int i = 0; i < best.length; i++){
		//	System.out.print(best[i] + ", ");
		//}
		//System.out.print(scoredPopulation.get(best));
		//System.out.println();
		
		scoredPopulation.clear();

		for(int i = 0; i < populationSize; i++){
			double[] child = copyAndMutate(best);
			unscoredPopulation.add(child);
		}
	
	}

	private double[] getBest(){
		double bestScore = minimize ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY;
		double[] best = null;

		for(double[] p : scoredPopulation.keySet()){
			double score = scoredPopulation.get(p);


			if( (minimize && score < bestScore) || (!minimize && score > bestScore)){
				bestScore = score;
				best = p;
			}
		}
		return best;
	}


	private double[] copyAndMutate(double[] parameters){
		double[] mutated = parameters.clone();

		int i = (int) (mutated.length * Math.random());
		int j = (int) (mutated.length * Math.random());
		
		while(j == i){
			j = (int) (mutated.length * Math.random());
		}
		
		
		mutated[i] = Math.random();
	//	mutated[j] = Math.random();

		return mutated;

	}

	public void setMinimize(boolean minimize) {
		this.minimize = minimize;
	}
}
