package com.staticvoidgames.framework.evolution;

import java.util.ArrayList;
import java.util.List;

public class Generation {

	
	
	List<Double> populationScores = new ArrayList<Double>();
	
	
	public double getMax(){
		double max = Double.NEGATIVE_INFINITY;
		for(double d : populationScores){
			if(d > max){
				max = d;
			}
		}

		return max;
	}
	
	
	public double getAverage(){
		double avg = 0;
		for(double d : populationScores){
			avg += d;
		}
		avg /= populationScores.size();
		return avg;
	}
	
	public void add(double p){
		populationScores.add(p);
	}

	public int getSize() {
		return populationScores.size();
	}
	
}
