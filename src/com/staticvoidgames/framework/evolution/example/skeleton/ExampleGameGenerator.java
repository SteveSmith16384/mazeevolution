package com.staticvoidgames.framework.evolution.example.skeleton;

public class ExampleGameGenerator {
	
	double[] parameters;

	public ExampleGameGenerator(double[] parameters) {
		this.parameters = parameters;
	}

	public GameLevel generateGameLevel(){
		
		double difficulty = 0;
		
		for(int i =0; i < parameters.length; i++){
			difficulty += parameters[i];
		}
		
		GameLevel gameLevel = new GameLevel(difficulty);
		return gameLevel;
	}

	
	
}
