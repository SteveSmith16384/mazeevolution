package com.staticvoidgames.framework.evolution.example.skeleton;

import com.staticvoidgames.framework.evolution.EvolutionFramework;

public class ExampleMain {

	
	private EvolutionFramework evolutionFramework = new EvolutionFramework(10);
	
	
	public ExampleMain(){
		
		
		
		for(int i = 0; i < 200; i++){
			double[] parameters = evolutionFramework.getNext();
			
			ExampleGameGenerator gameGenerator = new ExampleGameGenerator(parameters);
			GameLevel level = gameGenerator.generateGameLevel();
			
			GameLevelEvaluator evaluator = new GameLevelEvaluator();
			double fitness = evaluator.getFitness(level);
			
			evolutionFramework.setScore(parameters, fitness);
			
			
		}
		
	}
	
	public static void main(String... args){
		
	
		new ExampleMain();
	}
	
	
	
}
