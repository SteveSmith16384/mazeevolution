package com.staticvoidgames.framework.evolution.example.skeleton;

public class GameLevel {
	
	private double difficulty;
	
	public GameLevel(double difficulty){
		this.difficulty = difficulty;
	}
	
	
	public double getDifficulty(){
		return difficulty;
	}
	

}
