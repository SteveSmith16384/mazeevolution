package com.staticvoidgames.maze;

import java.util.List;
import java.util.ArrayList;

public class MazeSolver {
	
	MazeLevel level;

	List<Cell> checked = new ArrayList<Cell>();
	
	public int solve(MazeLevel level){
		this.level = level;
		
		Cell start = level.cells[level.entranceColumn][level.entranceRow];
		
		return solve(start, 0);
	}
	
	
	private int solve(Cell c, int path){
		
		if(c.isExit){
			return path;
		}
		
		checked.add(c);
		List<Cell> neighbors = getNeighbors(c);
		
		for(Cell n : neighbors){
			if(checked.contains(n)){
				continue;
			}
			
			int p = solve(n, path+1);
			
			if(p != -1){
				return p;
			}
		}
		
		
		return -1;
		
	}
	
	
	private List<Cell> getNeighbors(Cell c){
		List<Cell> neighbors = new ArrayList<Cell>();
		
		if(!c.topWall){
			neighbors.add(level.cells[c.column][c.row-1]);
		}
		
		if(!c.bottomWall){
			neighbors.add(level.cells[c.column][c.row+1]);
		}
		
		if(!c.leftWall){
			neighbors.add(level.cells[c.column-1][c.row]);
		}
		
		if(!c.rightWall){
			neighbors.add(level.cells[c.column+1][c.row]);
		}
		
		return neighbors;
		
	}
	
	
}
