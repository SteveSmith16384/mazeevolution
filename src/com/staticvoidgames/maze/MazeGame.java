package com.staticvoidgames.maze;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.staticvoidgames.framework.evolution.EvolutionFramework;

@SuppressWarnings("serial")
public class MazeGame extends JApplet {
	
	public static void main(String... args){

		int generationSize = 3;

		EvolutionFramework 	framework = new EvolutionFramework(10, generationSize);
		framework.setMinimize(false);

		JFrame frame = new JFrame("Maze Evolution");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Maze maze = new Maze(framework);

		JScrollPane logScrollPane = new JScrollPane(maze.log);
		logScrollPane.setMinimumSize(new Dimension(500, 100));
		logScrollPane.setMaximumSize(new Dimension(500, 100));
		logScrollPane.setPreferredSize(new Dimension(500, 100));

		frame.add(maze);
		frame.add(logScrollPane, BorderLayout.SOUTH);
		frame.add(maze.timeLabel, BorderLayout.NORTH);
		
		frame.setSize(500, 600);
		
		maze.setFocusable(true);
		maze.requestFocusInWindow();

		frame.setVisible(true);
	}
	

	public void init(){
		JPanel panel = new JPanel(new BorderLayout());
		
		int generationSize = 3;

		EvolutionFramework 	framework = new EvolutionFramework(10, generationSize);
		framework.setMinimize(false);

		Maze maze = new Maze(framework);

		JScrollPane logScrollPane = new JScrollPane(maze.log);
		logScrollPane.setMinimumSize(new Dimension(500, 100));
		logScrollPane.setMaximumSize(new Dimension(500, 100));
		logScrollPane.setPreferredSize(new Dimension(500, 100));

		panel.add(maze, BorderLayout.CENTER);
		panel.add(logScrollPane, BorderLayout.SOUTH);

		panel.add(maze.timeLabel, BorderLayout.NORTH);

		maze.setFocusable(true);
		maze.requestFocusInWindow();
		
		setContentPane(panel);
	}
	
}
