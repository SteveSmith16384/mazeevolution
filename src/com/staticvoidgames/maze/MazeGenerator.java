package com.staticvoidgames.maze;

public class MazeGenerator{
	
	public MazeLevel generateLevel(double[] parameters) {
		
		int length = 25;//(int) (10 + 90 * parameters[0]);//25;
		
		int goalEntranceColumn = (int) (length * parameters[0]);
		int goalEntranceRow = (int) (length*parameters[1]);
		
		int goalExitColumn = (int) (length * parameters[2]);
		int goalExitRow = (int) (length*parameters[3]);
		
		double leftChance = parameters[4];
		double rightChance = parameters[5];
		double straightChance  = parameters[6];
		
		double selectionMultiplier = parameters[7];
		
		double algorithmStartColumnMultiplier = parameters[8]; // Math.random();//parameters[9];
		double algorithmStartRowMultiplier = parameters[9];// Math.random();//parameters[10];
		
		
		MazeLevel level = new MazeLevel(length,
				goalEntranceColumn,	goalEntranceRow,
				goalExitColumn, goalExitRow,
				leftChance, rightChance, straightChance,
				selectionMultiplier,
				algorithmStartColumnMultiplier,
				algorithmStartRowMultiplier);
		
				return level;
	}

}
