package com.staticvoidgames.maze;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MazeLevel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public long time_created; // So we know who's maze to use.
	
	int columns;
	int rows; 

	int entranceColumn;
	int entranceRow;

	int exitColumn;
	int exitRow;

	Cell[][] cells;

	public MazeLevel(int length,
			int goalEntranceColumn,	int goalEntranceRow,
			int goalExitColumn, int goalExitRow,
			double leftChance, double rightChance, double straightChance,
			double selectionMultiplier,
			double algorithmStartColumnMultiplier,
			double algorithmStartRowMultiplier){


		this.columns = length;
		this.rows = length;

		cells = new Cell[columns][rows];

		for(int c = 0; c < columns; c++){
			for(int r = 0; r < rows; r++){
				cells[c][r] = new Cell(c, r, columns, rows);
			}
		}



		List<Cell> doneCells = new ArrayList<Cell>();
		List<Cell> cellsToDo = new ArrayList<Cell>();

		//50 50

		int algorithmStartRow = (int) (rows * algorithmStartRowMultiplier);
		int algorithmStartColumn = (int) (columns * algorithmStartColumnMultiplier);

		
		
		
		

		cells[algorithmStartColumn][algorithmStartRow].direction = Cell.Direction.UP;

		doneCells.add(cells[algorithmStartColumn][algorithmStartRow]);

		if(algorithmStartColumn-1 >= 0){
			cells[algorithmStartColumn][algorithmStartRow].leftWall = false;
			cells[algorithmStartColumn-1][algorithmStartRow].direction = Cell.Direction.LEFT;
			cellsToDo.add(cells[algorithmStartColumn-1][algorithmStartRow]);
			cells[algorithmStartColumn-1][algorithmStartRow].knockDownEntranceWall();
		}

		if(algorithmStartColumn+1 < columns){
			cells[algorithmStartColumn][algorithmStartRow].rightWall = false;
			cells[algorithmStartColumn+1][algorithmStartRow].direction = Cell.Direction.RIGHT;
			cellsToDo.add(cells[algorithmStartColumn+1][algorithmStartRow]);
			cells[algorithmStartColumn+1][algorithmStartRow].knockDownEntranceWall();
		}

		if(algorithmStartRow-1 >= 0){
			cells[algorithmStartColumn][algorithmStartRow].topWall = false;
			cells[algorithmStartColumn][algorithmStartRow-1].direction = Cell.Direction.UP;
			cellsToDo.add(cells[algorithmStartColumn][algorithmStartRow-1]);
			cells[algorithmStartColumn][algorithmStartRow-1].knockDownEntranceWall();
		}

		if(algorithmStartRow+1 < rows){
			cells[algorithmStartColumn][algorithmStartRow].bottomWall = false;
			cells[algorithmStartColumn][algorithmStartRow+1].direction = Cell.Direction.DOWN;
			cellsToDo.add(cells[algorithmStartColumn][algorithmStartRow+1]);
			cells[algorithmStartColumn][algorithmStartRow+1].knockDownEntranceWall();
		}



		Random r = new Random(123456789);


		while(!cellsToDo.isEmpty()){

			//Cell c = cellsToDo.get((int) (cellsToDo.size() * Math.random()));
			//	Cell c = cellsToDo.get(0);
			//Cell c = cellsToDo.get(cellsToDo.size() -1);
			Cell c = cellsToDo.get((int) (cellsToDo.size() * selectionMultiplier));

			if(r.nextDouble() < leftChance){
				Cell leftNeighbor = getLeftNeighbor(c);
				if(leftNeighbor != null && !leftNeighbor.done && !cellsToDo.contains(leftNeighbor)){
					leftNeighbor.direction = Cell.Direction.rotateLeft(c.direction);

					c.knockDownLeftWall();
					leftNeighbor.knockDownEntranceWall();


					cellsToDo.add(leftNeighbor);
				}
			}


			if(r.nextDouble() < rightChance){
				Cell rightNeighbor = getRightNeighbor(c);
				if(rightNeighbor != null && !rightNeighbor.done && !cellsToDo.contains(rightNeighbor)){
					rightNeighbor.direction = Cell.Direction.rotateRight(c.direction);

					c.knockDownRightWall();
					rightNeighbor.knockDownEntranceWall();


					cellsToDo.add(rightNeighbor);
				}
			}

			if(r.nextDouble() < straightChance){
				Cell straightNeighbor = getStraightNeighbor(c);
				if(straightNeighbor != null && !straightNeighbor.done && !cellsToDo.contains(straightNeighbor)){
					straightNeighbor.direction = c.direction;

					c.knockDownStraightWall();
					straightNeighbor.knockDownEntranceWall();


					cellsToDo.add(straightNeighbor);
				}
			}

			c.done = true;
			doneCells.add(c);
			cellsToDo.remove(c);

		} //cellsToDo is empty




		applyEntrance(goalEntranceColumn, goalEntranceRow);
		applyExit(goalExitColumn, goalExitRow);



		this.time_created = System.currentTimeMillis();
	}

	
	public void render(Graphics g, int width, int height){
		g.setColor(Color.BLACK);

		for(int c = 0; c < columns; c++){
			for(int r = 0; r < rows; r++){
				cells[c][r].render(g, width, height);

			}
		}
	}

	public int getEntranceColumn(){
		return entranceColumn;
	}

	public int getEntranceRow(){
		return entranceRow;
	}

	public int getExitColumn(){
		return exitColumn;
	}

	public int getExitRow(){
		return exitRow;
	}



	private void applyEntrance(int goalEntranceColumn, int goalEntranceRow){

		int searchRadius = 0;

		//find square closest to goal entrance
		while(true){
			for(int c = goalEntranceColumn-searchRadius; c <= goalEntranceColumn + searchRadius; c++){

				if(c < 0 || c >= columns){
					continue;
				}

				for(int r = goalEntranceRow-searchRadius; r <= goalEntranceRow + searchRadius; r++){
					if(r < 0 || r >= rows){
						continue;
					}

					if(cells[c][r].done){
						cells[c][r].isEntrance = true;
						entranceColumn = c;
						entranceRow = r;
						return;
					}

				}
			}
			searchRadius++;
		}
	}

	private void applyExit(int goalExitColumn, int goalExitRow){


		int searchRadius = 0;

		//find square closest to goal entrance
		while(true){
			for(int c = goalExitColumn-searchRadius; c <= goalExitColumn + searchRadius; c++){

				if(c < 0 || c >= columns){
					continue;
				}

				for(int r = goalExitRow-searchRadius; r <= goalExitRow + searchRadius; r++){
					if(r < 0 || r >= rows){
						continue;
					}

					if(cells[c][r].done){
						cells[c][r].isExit = true;
						exitColumn = c;
						exitRow = r;
						return;
					}

				}
			}
			searchRadius++;
		}

	}


	private Cell getStraightNeighbor(Cell c){
		int dc = 0;
		int dr = 0;

		if(c.direction == Cell.Direction.UP){
			dr = -1;
		}
		else if(c.direction == Cell.Direction.DOWN){
			dr = 1;
		}
		else if(c.direction == Cell.Direction.LEFT){
			dc = -1;
		}
		else if(c.direction == Cell.Direction.RIGHT){
			dc = 1;
		}

		int newRow = c.row + dr;
		int newCol = c.column + dc;

		if(newRow < 0 || newRow >= rows){
			return null;
		}

		if(newCol < 0 || newCol >= columns){
			return null;
		}

		return cells[newCol][newRow];
	}


	private Cell getLeftNeighbor(Cell c){
		int dc = 0;
		int dr = 0;

		if(c.direction == Cell.Direction.UP){
			dc = -1;
		}
		else if(c.direction == Cell.Direction.DOWN){
			dc = 1;
		}
		else if(c.direction == Cell.Direction.LEFT){
			dr = 1;
		}
		else if(c.direction == Cell.Direction.RIGHT){
			dr = -1;
		}

		int newRow = c.row + dr;
		int newCol = c.column + dc;

		if(newRow < 0 || newRow >= rows){
			return null;
		}

		if(newCol < 0 || newCol >= columns){
			return null;
		}

		return cells[newCol][newRow];
	}

	private Cell getRightNeighbor(Cell c){
		int dc = 0;
		int dr = 0;

		if(c.direction == Cell.Direction.UP){
			dc = 1;
		}
		else if(c.direction == Cell.Direction.DOWN){
			dc = -1;
		}
		else if(c.direction == Cell.Direction.LEFT){
			dr = -1;
		}
		else if(c.direction == Cell.Direction.RIGHT){
			dr = 1;
		}

		int newRow = c.row + dr;
		int newCol = c.column + dc;

		if(newRow < 0 || newRow >= rows){
			return null;
		}

		if(newCol < 0 || newCol >= columns){
			return null;
		}

		return cells[newCol][newRow];
	}





}
