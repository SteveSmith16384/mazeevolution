package com.staticvoidgames.maze;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;


public class Cell implements Serializable {

	private static final long serialVersionUID = 1L;

	enum Direction{
		UP, DOWN, LEFT, RIGHT;
		
		
		public static Direction rotateLeft(Direction dir){
			if(dir.equals(UP)){
				return LEFT;
			}
			if(dir.equals(LEFT)){
				return DOWN;
			}
			if(dir.equals(DOWN)){
				return RIGHT;
			}
			return UP;
		}
		
		public static Direction rotateRight(Direction dir){
			if(dir.equals(UP)){
				return RIGHT;
			}
			if(dir.equals(RIGHT)){
				return DOWN;
			}
			if(dir.equals(DOWN)){
				return LEFT;
			}
			return UP;
		}
	}
	
	
	boolean topWall = true;
	boolean bottomWall = true;
	boolean leftWall = true;
	boolean rightWall = true;
	
	boolean done = false;
	
	Direction direction;
	
	int row;
	int column;
	
	int totalColumns;
	int totalRows;
	
	boolean isEntrance = false;
	boolean isExit = false;
	
	boolean visited = false;
	
	public Cell(int column, int row, int totalColumns, int totalRows){
		this.row = row;
		this.column = column;
		
		this.totalRows = totalRows;
		this.totalColumns = totalColumns;
	}
	
	
	public void render(Graphics g, double pWidth, double pHeight){
		
		if(leftWall && rightWall && topWall && bottomWall){
			return;
		}
		
		//g.setColor(new Color((int)(Math.random()*255), (int)(Math.random()*255), (int)(Math.random()*255)));
		
		
		double width = pWidth/totalColumns;
		double height = pHeight/totalRows;
		
		double x = ((double)column/totalColumns) * pWidth;
		double y = ((double)row/totalRows) * pHeight;
		
		int wallDiv = 8;
		
		
		if(visited){
			g.setColor(Color.CYAN);
			g.fillRect((int)x-1, (int)y-1, (int)width+1, (int)height+1);
		}
		
		if(isEntrance){
			g.setColor(Color.GREEN);
			g.fillRect((int)x-1, (int)y-1, (int)width+1, (int)height+1);
		}
		
		if(isExit){
			Color[] colors = {Color.BLUE, Color.CYAN, Color.GREEN, Color.MAGENTA, Color.ORANGE, Color.PINK, Color.RED, Color.WHITE, Color.YELLOW};
			
			
			for(int i = 0; i < 10; i++){
				g.setColor(colors[(int) (Math.random() * colors.length)]);
				g.fillRect((int)x-1+i, (int)y-1+i, (int)width+1-i*2, (int)height+1-i*2);
			}
		}
		
		
		
		
		
		g.setColor(Color.BLACK);
		if(leftWall){
			g.fillRect((int)x-1, (int)y-1, (int)(width/wallDiv+1), (int)height+1);
		}
		if(rightWall){
			g.fillRect((int)(x-1 + width - width/wallDiv), (int)y-1, (int)(width/wallDiv+1), (int)height+1);
		}
		
		if(topWall){
			g.fillRect((int)x-1, (int)y-1, (int)width+1, (int)(height/wallDiv)+1);
		}
		if(bottomWall){
			g.fillRect((int)x-1, (int)(y-1 + height - height/wallDiv), (int)(width+1), (int)(height/wallDiv)+1);
		}
		
		
		
	}
	
	public void knockDownStraightWall(){
		

		
		if(direction == Direction.UP){
			topWall = false;
		}
		else if(direction == Direction.DOWN){
			bottomWall = false;
		}
		else if(direction == Direction.LEFT){
			leftWall = false;
		}
		else if(direction == Direction.RIGHT){
			rightWall = false;
		}
		
	}
	
	public void knockDownLeftWall(){
		
		Direction wallDirection = Direction.rotateLeft(direction);
		
		if(wallDirection == Direction.UP){
			topWall = false;
		}
		else if(wallDirection == Direction.DOWN){
			bottomWall = false;
		}
		else if(wallDirection == Direction.LEFT){
			leftWall = false;
		}
		else if(wallDirection == Direction.RIGHT){
			rightWall = false;
		}
		
	}
	
	public void knockDownRightWall(){
		
		Direction wallDirection = Direction.rotateRight(direction);
		
		if(wallDirection == Direction.UP){
			topWall = false;
		}
		else if(wallDirection == Direction.DOWN){
			bottomWall = false;
		}
		else if(wallDirection == Direction.LEFT){
			leftWall = false;
		}
		else if(wallDirection == Direction.RIGHT){
			rightWall = false;
		}
		
	}
	
	public void knockDownEntranceWall(){
		if(direction == Direction.UP){
			bottomWall = false;
		}
		else if(direction == Direction.DOWN){
			topWall = false;
		}
		else if(direction == Direction.LEFT){
			rightWall = false;
		}
		else if(direction == Direction.RIGHT){
			leftWall = false;
		}
		
	}
	
}
