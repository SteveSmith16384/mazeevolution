package com.staticvoidgames.maze;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.Timer;

import com.scs.gmc.ConnectorMain;
import com.scs.gmc.ConnectorMain.GameStage;
import com.scs.gmc.IGameClient;
import com.scs.gmc.StartGameOptions;
import com.staticvoidgames.framework.evolution.EvolutionFramework;
import com.staticvoidgames.framework.evolution.Generation;

import ssmith.io.Serialization;


public class Maze extends JPanel implements IGameClient {

	// Network codes
	private static final int FINISHED_MAZE = 1; // Send time as well

	private Player player = new Player();
	private MazeLevel current_level;
	private EvolutionFramework framework;
	private long levelStartTime;
	private double[] currentParameters;
	public JLabel timeLabel = new JLabel();
	public JTextArea log = new JTextArea();
	private boolean intro = true;

	private ConnectorMain connector;

	public Maze(final EvolutionFramework framework) {
		super();

		this.framework = framework;

		connector = StartGameOptions.ShowOptionsAndConnect(this, "MazEvolution", null);
		if (connector == null) {
			System.exit(0);
		}
		log("Connected to server " + connector.getServer() + ":" + connector.getPort());
		log("Hello " + connector.getPlayerName());
		log("Joined game " + connector.getGameID());

		framework.setMaze(this);

		timeLabel.setOpaque(true);
		timeLabel.setHorizontalAlignment(JLabel.CENTER);

		log.setFocusable(false);
		log.setEditable(false);

		setBackground(Color.WHITE);

		final Timer timer = new Timer(100, new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(!intro){
					long elapsed = System.currentTimeMillis() - levelStartTime;
					double remain = elapsed/1000.0;
					DecimalFormat df = new DecimalFormat("00.0");
					timeLabel.setText(df.format(remain));

					int r = (int) (255 * (elapsed / 60000.0));
					r = Math.max(Math.min(r, 255), 0);

					timeLabel.setBackground(new Color(255, 255-r, 255-r));
				}
				repaint();
			}
		});


		addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent e) {
				if (connector.getGameStage() == GameStage.WAITING_FOR_PLAYERS) {
					log("Still waiting for players.");
					return;
				}

				if (intro) {
					startGame();
					return;
				}

				if (current_level == null) {
					log("Still waiting for maze.");
					return;
				}

				current_level.cells[player.column][player.row].visited = true;

				if(e.getKeyCode() == KeyEvent.VK_LEFT){
					if(!current_level.cells[player.column][player.row].leftWall){
						player.column--;
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_RIGHT){
					if(!current_level.cells[player.column][player.row].rightWall){
						player.column++;
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_UP){
					if(!current_level.cells[player.column][player.row].topWall){
						player.row--;
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_DOWN){
					if(!current_level.cells[player.column][player.row].bottomWall){
						player.row++;
					}
				}

				if(player.column == current_level.getExitColumn() && player.row == current_level.getExitRow()) {
					levelComplete(true);
				}

				repaint();
			}

			@Override
			public void keyReleased(KeyEvent e) {}

			@Override
			public void keyTyped(KeyEvent e) {}

		});

		timer.start();

		try {
			connector.joinGame();
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(0);
		}
		//log("Joined game.");


		MazeLevel maze = this.generateMaze(10);
		this.setMazeLevel(maze);

	}


	private void startGame() {
		log("Creating initial random mazes.");

		try {
			if (this.connector.areWeFirstPlayer()) {
				log("Sending map out (start of game)");
				this.connector.sendObjectByTCP(this.current_level);
			}
		} catch (IOException e) {
			log(e.getMessage());
			e.printStackTrace();
		}

		intro = false;


	}

	public void levelComplete(boolean itWasUs) {
		long elapsed = System.currentTimeMillis() - levelStartTime;

		if (itWasUs) {
			try {
				connector.sendKeyValueDataByTCP(FINISHED_MAZE, (int)(elapsed/1000));
				connector.sendStringDataByTCP(this.connector.getPlayerName() + " has completed the maze!");
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		generateAndSendNextLevel(elapsed);
	}


	public void generateAndSendNextLevel(double score) {
		if (this.connector.areWeFirstPlayer()) {
			MazeLevel l = this.generateMaze(score);
			try {
				log("Sending map out (new level)");
				this.connector.sendObjectByTCP(l);
			} catch (IOException e) {
				log(e.getMessage());
				e.printStackTrace();
			}
			setMazeLevel(l);
		}
	}


	private MazeLevel generateMaze(double score) {
		framework.setScore(currentParameters, score);
		MazeGenerator generator = new MazeGenerator();
		currentParameters = framework.getNext();
		MazeLevel firstLevel = generator.generateLevel(currentParameters);

		MazeSolver solver = new MazeSolver();
		int path = solver.solve(firstLevel);
		if(path < 20){
			log("Created a small maze, skipping it.");
			return generateMaze(score/10.0);
		}
		return firstLevel;
	}


	public void setMazeLevel(MazeLevel level) {
		this.current_level = level;
		player.setColumn(level.getEntranceColumn());
		player.setRow(level.getEntranceRow());

		levelStartTime = System.currentTimeMillis();
		repaint();
	}


	public void paintComponent(Graphics g){
		super.paintComponent(g);

		if(intro){
			g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 24));
			g.drawString("MazEvolution", 90, 50);

			g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
			g.drawString("This \"game\" uses an evolutionary strategy", 70, 100);
			g.drawString("to evolve more difficult mazes over time.", 90, 120);

			g.drawString("The game starts out with random (bad) mazes.", 60, 200);
			g.drawString("The maze that takes you the longest to solve", 60, 220);
			g.drawString("becomes the parent of the next generation of mazes.", 50, 240);

			g.drawString("Over time, the mazes should get harder.", 70, 260);
			if (connector.getGameStage() == GameStage.IN_PROGRESS) {
				g.drawString("Press any key to start!", 150, 450);
			} else if (connector.getGameStage() == GameStage.FINISHED) {
				g.drawString("Game finished!", 150, 450);
			} else {
				g.drawString("Waiting for players...", 150, 450);
			}

			int cellWidth = 50;
			int cellHeight = 50;

			int x = 250;
			int y = 12;

			g.setColor(Color.MAGENTA);
			g.fillOval(x+1, y+1, cellWidth-4, cellHeight-3);

			g.setColor(Color.BLACK);

			if(Math.random() < .05){
				g.fillRect((int)(x+1+(cellWidth*.25)), (int)(y+1 + (cellHeight * .25) + (cellHeight*.25/2)), (int)(cellWidth*.25), (int)(cellHeight*.1));
				g.fillRect((int)(x+1+(cellWidth*.50)), (int)(y+1 + (cellHeight * .25)+ (cellHeight*.25/2)), (int)(cellWidth*.25), (int)(cellHeight*.1));
			}
			else{
				g.fillOval((int)(x+1+(cellWidth*.25)), (int)(y+1 + (cellHeight * .25)), (int)(cellWidth*.25), (int)(cellHeight*.25));
				g.fillOval((int)(x+1+(cellWidth*.50)), (int)(y+1 + (cellHeight * .25)), (int)(cellWidth*.25), (int)(cellHeight*.25));
			}

			if(Math.random() < .05){
				g.fillOval((int)(x+1+(cellWidth*.5 - (cellWidth*.25/2))), (int)(y-1 + (cellHeight * .75) - (cellHeight*.25/2)  ), (int)(cellWidth*.25), (int)(cellHeight*.25));
			}
			else{
				g.fillRect((int)(x+1+(cellWidth*.25)), (int)(y-1 + (cellHeight * .75)), (int)(cellWidth*.5), (int)(cellHeight*.1));
			}


			return;
		}


		try {
			if (current_level != null) {
				current_level.render(g, getWidth(), getHeight());

				int cellWidth = getWidth()/current_level.columns;
				int cellHeight = getHeight()/current_level.rows;

				int x = (int) (((double)player.column/current_level.columns) * getWidth());
				int y = (int) (((double)player.row/current_level.rows) * getHeight());

				g.setColor(Color.MAGENTA);
				g.fillOval(x+1, y+1, cellWidth-4, cellHeight-3);

				g.setColor(Color.BLACK);

				if(Math.random() < .05){
					g.fillRect((int)(x+1+(cellWidth*.25)), (int)(y+1 + (cellHeight * .25) + (cellHeight*.25/2)), (int)(cellWidth*.25), (int)(cellHeight*.1));
					g.fillRect((int)(x+1+(cellWidth*.50)), (int)(y+1 + (cellHeight * .25)+ (cellHeight*.25/2)), (int)(cellWidth*.25), (int)(cellHeight*.1));
				}
				else{
					g.fillOval((int)(x+1+(cellWidth*.25)), (int)(y+1 + (cellHeight * .25)), (int)(cellWidth*.25), (int)(cellHeight*.25));
					g.fillOval((int)(x+1+(cellWidth*.50)), (int)(y+1 + (cellHeight * .25)), (int)(cellWidth*.25), (int)(cellHeight*.25));
				}

				if(Math.random() < .05){
					g.fillOval((int)(x+1+(cellWidth*.5 - (cellWidth*.25/2))), (int)(y-1 + (cellHeight * .75) - (cellHeight*.25/2)  ), (int)(cellWidth*.25), (int)(cellHeight*.25));
				}
				else{
					g.fillRect((int)(x+1+(cellWidth*.25)), (int)(y-1 + (cellHeight * .75)), (int)(cellWidth*.5), (int)(cellHeight*.1));
				}
			}
		} catch (NullPointerException ex) {
			// The current level may have been set to null while we're drawing it
		}
	}

	public void log(String text){
		String currentText = log.getText();
		currentText += "\n";
		currentText += text;
		log.setText(currentText);
		log.setCaretPosition(currentText.length());
	}


	public static void main2(String... args){

		int sampleSize = 100;
		int generationSize = 3;
		//don't look at just the last bit!!!
		//5 = 106
		//6 = 132, 133
		//7 = 179
		//8 = 226
		//9 = 272
		//10 = 290, 281
		//11 = 296
		//12 = 311
		//13 = 311
		//14 = 299
		//15 = 293
		//16 =
		//17 =
		//18 = 
		//19 =
		//20 = 244
		List<List<Generation>> generationsList = new ArrayList<List<Generation>>();

		for(int t = 0; t < sampleSize; t++){

			EvolutionFramework 	framework = new EvolutionFramework(10, generationSize);
			framework.setMinimize(false);


			List<Generation> generationList = new ArrayList<Generation>();
			generationsList.add(generationList);



			//HistoryDisplay history = new HistoryDisplay(generationSize);
			System.out.println("t: " + t);

			Generation g = new Generation();


			int birth = -1;

			for(int i = 0; i < 10000; i++){

				birth++;

				if(birth == generationSize){
					g = new Generation();
					generationList.add(g);
					birth = 0;
				}


				MazeGenerator generator = new MazeGenerator();

				double[] currentParameters = framework.getNext();


				MazeLevel level = generator.generateLevel(currentParameters);

				MazeSolver solver = new MazeSolver();


				int score = solver.solve(level);

				g.add(score);

				framework.setScore(currentParameters, score);
				//history.addLevel(level, score);

			}
		}


		for(int i = 0; i < generationsList.get(0).size(); i++){

			double totalAvg = 0;
			double maxAvg = 0;

			for(int t = 0; t < sampleSize; t++){
				Generation generation = generationsList.get(t).get(i);
				//System.out.println("Size: " + generation.getSize());
				double avg = generation.getAverage();
				double max = generation.getMax();

				//System.out.print(max);
				//System.out.print(avg);
				//System.out.print("\t");

				maxAvg += max;
				totalAvg+= avg;
			}
			totalAvg /= sampleSize;
			maxAvg /= sampleSize;
			//System.out.println(totalAvg);
			System.out.println(maxAvg);

		}

	}


	public static void main3(String... args){
		int sampleSize = 100;
		int generationSize = 8;

		EvolutionFramework 	framework = new EvolutionFramework(10, generationSize);
		framework.setMinimize(false);

		double score = 0;

		while (score < 100) {
			MazeGenerator generator = new MazeGenerator();
			double[] currentParameters = framework.getNext();
			MazeLevel level = generator.generateLevel(currentParameters);
			MazeSolver solver = new MazeSolver();
			score = solver.solve(level);
			framework.setScore(currentParameters, score);
			//history.addLevel(level, score);
		}
	}


	// GMC stuff....

	@Override
	public void stringReceivedByTCP(int arg0, String text) {
		log(text);
	}


	@Override
	public void byteArrayReceivedByTCP(int arg0, byte[] b) {

	}

	@Override
	public void keyValueReceivedByTCP(int fromplayerid, int key, int value) throws IOException {
		if (key == FINISHED_MAZE) {
			this.levelComplete(false);
			//log("An opponent has completed the level!");
		}

	}

	@Override
	public void stringReceivedByUDP(long arg0, int arg1, String arg2) {

	}

	@Override
	public void byteArrayReceivedByUDP(long arg0, int arg1, byte[] arg2) {

	}

	@Override
	public void keyValueReceivedByUDP(long arg0, int arg1, int arg2, int arg3) throws IOException {
	}

	@Override
	public void error(Throwable ex) {
		log("Error from server: " + ex.getMessage());

	}


	@Override
	public void gameEnded(String winner) {

	}

	@Override
	public void gameStarted() {
		startGame();

	}

	@Override
	public void playerJoined(String name) {
		try {
			if (this.connector.areWeFirstPlayer()) {
				log("Sending map out (new player)");
				this.connector.sendObjectByTCP(this.current_level);
			}
		} catch (IOException e) {
			log(e.getMessage());
			e.printStackTrace();
		}

	}

	@Override
	public void playerLeft(String name) {
		log(name + " has left");

	}

	@Override
	public void serverDown(long arg0) {

	}


	@Override
	public void objectReceivedByTCP(int fromplayerid, Object o) throws IOException {
		if (o instanceof MazeLevel) {
			MazeLevel new_level = (MazeLevel)o;
			if (this.current_level == null || new_level.time_created != this.current_level.time_created) {
				log("Received new map");
				this.setMazeLevel(new_level);
			}
		} else {
			throw new ClassCastException("Unhandled class: " + o);
		}
	}


	@Override
	public void objectReceivedByUDP(long time, int fromplayerid, Object obj) throws IOException {

	}


}
