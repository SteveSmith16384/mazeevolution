# Maze Evolution

A multiplayer maze game.  Original game by Kevin Workman (http://staticvoidgames.com/).  Multiplayer aspect by Stephen Carlyle-Smith (https://twitter.com/stephencsmith).


## Overview
Maze Evolution is a simple multiplayer maze game.  Each player is given a randomly generated maze and tries to be the first to get to the exit.  When one player reaches the exit, all the players get a new level, but the players who didn't get to the exit will get a more complicated maze.


## Controls
Use the arrow keys to move.


##Licence
Source code licenced under GPL v3 (as per https://github.com/KevinWorkman/StaticVoidGames).